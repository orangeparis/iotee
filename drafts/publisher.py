# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt


# broker = "iot.eclipse.org"
broker = "192.168.99.100"

mqttc = mqtt.Client("python_pub")

mqttc.connect(broker, 1883)

mqttc.publish("hello/world", "Hello, World!")

mqttc.loop(2)  # timeout = 2s


