# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt



# broker = "iot.eclipse.org"
broker = "192.168.99.100"


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("$SYS/#")
    client.subscribe("hello/world")
    client.subscribe("alert")
    client.subscribe("demo/#")


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):

    # topic = str(msg.topic)
    # print(topic)

    try:
        topic = str(msg.topic)
    except UnicodeDecodeError:
        return
    # print(topic)

    if msg.topic.startswith("$SYS/"):
        return
    else:
        print(msg.topic+" "+str(msg.payload))

    # print(msg.topic+" "+msg.payload.decode('utf-8'))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
