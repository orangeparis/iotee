import toml
from pprint import pprint
from iotee.emiter import Emiter,Run



class Config(dict):
    """


    """
    state = {}
    filename = ""

    def __init__(self, state):
        """

        :param filename:
        """
        super(Config, self).__init__()
        #self.__dict__ = state

    @classmethod
    def create(cls, filename):
        """

        :param filename:
        :return:
        """
        if not cls.state:
            # create it
            cls.filename = filename
            fh = open(filename, "r")
            cls.state = toml.loads(fh.read())
            return cls(cls.state)
        else:
            raise RuntimeError("config already created")

    @classmethod
    def get_instance(cls):
        """

        :return:
        """
        if cls.state:
            conf = cls(cls.state)
            return conf
        else:
            raise RuntimeError("no config created")

    def show(self):
        """

        :return:
        """
        pprint(self.state)

    def message_data(self, name):
        """

        :param name:
        :return:
        """
        message_data = self.state["message"][name]
        return message_data

    def emiter(self, name, duration=10):
        """

        :param name:
        :param duration:
        :return: instance of Emiter
        """

        emiter_data = self.state["emiter"][name]

        topic = emiter_data["topic"]
        frequency = emiter_data["frequency"]
        msg_id = emiter_data["message"]

        message_data = self.message_data(msg_id)
        host = self.state["broker"]["host"]

        e = Emiter(name= name, host=host, topic=topic, message=message_data, frequency=frequency, duration=duration)
        return e

    def run(self, name, duration = None):
        """

        :param name:
        :param duration: int
        :return: a list of Emitter
        """


        # emiters = []
        # run_data = self.state["run"][name]
        #
        # duration= duration or run_data["duration"]
        # for name in run_data["emitters"]:
        #     e = self.emiter(name, duration=duration)
        #     emiters.append(e)
        # return emiters

        run = Run(self,name=name,state=self.state["run"][name],duration=duration)
        return run

