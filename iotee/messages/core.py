import time
import json


class MessageBase(object):
    """




    """
    def __init__(self, emiter: str, timestamp: int=0, event="event", text="", severity="low",):
        """

        :param timestamp:
        :param emiter:
        :param event:
        :param text:
        :param severity:
        """
        if not timestamp:
            #timestamp = int(time.time())
            timestamp = time.time()
        self.state = {
            "timestamp": timestamp,
            "emiter": emiter,
            "event": event,
            "text": text,
            "severity": severity}


    @classmethod
    def from_json(cls, json_data: str):
        """

        :param json_data:
        :return:
        """
        data = json.loads(json_data)
        obj = cls(**data)
        return obj

    def to_json(self) -> str:
        """

        :return:
        """
        return json.dumps(self.state)