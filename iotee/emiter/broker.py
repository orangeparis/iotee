

import paho.mqtt.client as mqtt
import random

class Broker(object):
    """

    """
    def __init__(self,host,port=1883,timeout=2):
        """

        :param host:
        """
        self.host = host
        self.port = port
        self.timeout = timeout
        i= random.randint(1,1000000)
        self.client = mqtt.Client("python_pub%d" % i)

    def start(self):
        self.client.connect(self.host, self.port)
        self.client.loop(self.timeout)

