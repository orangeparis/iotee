
"""

   cnf = Config.create("conf.toml")
    host = cnf.state["broker"]["host"]

    cnf = Config.get_instance()

    duration = 2

    r1 = cnf.run("r1", duration=duration)
    emiters= []
    for e in r1:
        t = e.start()
        emiters.append(t)
    for e in emiters:
        e.join()

"""


class Run(object):
    """

    """
    def __init__(self, master, name, state, duration):
        """

        :param master:
        :param name:
        :param state:
        :param duration:
        """
        self.master = master
        self.name = name
        self.duration = duration
        self.emiters = []

        self.state = state

        duration= duration or self.state["duration"]
        for name in self.state["emitters"]:
            e = self.master.emiter(name, duration=duration)
            self.emiters.append(e)

    def start(self):
        """

        :return:
        """
        sons= []
        for e in self.emiters:
            t = e.start()
            sons.append(t)
        for e in sons:
            e.join()
        return
