import time
import threading
from iotee.messages import MessageBase
from .broker import Broker


class Emiter(object):
    """

    """
    def __init__(self, name, host, topic, message, frequency, duration):
        """

        :param host:
        :param message:
        """
        self.name = name
        self.host = host
        self.topic = topic

        self.frequency = frequency
        self.duration = duration

        if not isinstance(message, MessageBase):
            if not isinstance(message, dict):
                raise RuntimeError("message must be a dict")
            message= MessageBase("me", **message)
        self.message = message

        self.broker = Broker(host, timeout=2)
        self.payload = self.message.to_json()

        self.t0 = 0
        self.t_end = 0
        self.t_round = 0

    def start(self):
        #self.run()

        t = threading.Thread(target=self.run)
        t.start()
        #t.join()
        return t

    def run(self):
        self.t0= time.time()
        self.t_end = self.t0 + self.duration
        self.broker.start()
        self.loop()
        return True


    def run_once(self):
        """

        :return:
        """
        self.message.state["timestamp"]= time.time()
        self.payload = self.message.to_json()
        self.broker.client.publish(self.topic, self.payload)
        print(".")

    def loop(self):
        """

        :return:
        """
        t= time.time()
        while t < self.t_end:
            # publish
            t_round = time.time() + 1.0 / self.frequency
            print(t, t_round)
            self.run_once()
            # wait for frequency
            while 1:
                t= time.time()
                if t > t_round:
                    break
                time.sleep(0.000001)
            t = time.time()




